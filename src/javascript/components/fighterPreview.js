import { createElement } from '../helpers/domHelper';

export function isEmpty(obj) {
  for (let key in obj) {
    return false;
  }
  return true;
}


export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)   
  
  let viewInfo = !isEmpty(fighter) ? `
    <div class="fighter-preview__info">
      <div class="fighter-preview__info_img">
        <img src="${fighter.source}" alt="${fighter.name}">
      </div>
      <p class="fighter-preview__info_name"><strong>${fighter.name}</strong></p>
      <p class="fighter-preview__info_health">Health: ${fighter.health}</p>
      <p class="fighter-preview__info_defense">Defense: ${fighter.defense}</p>
      <p class="fighter-preview__info_attack">Attack: ${fighter.attack}</p>
    </div>
  ` : `
    <div class="fighter-preview__info">
      <div class="fighter-preview__info_choose">
        ?
      </div>
      <p class="fighter-preview__info_name"><strong>Сhoose a fighter</strong></p>
      
    </div>
  `;

  fighterElement.innerHTML = viewInfo;

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
