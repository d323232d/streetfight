import { controls } from '../../constants/controls';

const {PlayerOneAttack,
      PlayerOneBlock,
      PlayerTwoAttack,
      PlayerTwoBlock,
      PlayerOneCriticalHitCombination,
      PlayerTwoCriticalHitCombination} = controls;

function changeIndicatorWidth(initHelth, currentHelth) {
  return Math.floor(currentHelth / initHelth * 100) + '%'
};

function checkWinner(first, second, resolve) {  
  let winner = {};

  first.indicator = document.getElementById('left-fighter-indicator');
  second.indicator = document.getElementById('right-fighter-indicator');

  first.initHelth = first.health;
  second.initHelth = second.health;

  let firstFighter = {...first};
  let secondFighter = {...second};

  let pressed = new Map();
  
  
  document.addEventListener('keydown', function(e) {
    if (!pressed.has(e.code)) {
      pressed.set(e.code, true);
    }

    if(PlayerOneCriticalHitCombination.includes(e.code)) {
        let isCombo = PlayerOneCriticalHitCombination.every( item => pressed.get(item));
        if(!pressed.get(e.code)) {
          // ("Already bloked");
          return ;
        } else if(isCombo){
          // ('Q+W+E');
          secondFighter = {...criticalHit(firstFighter, secondFighter)};
        } 
    } else if(PlayerTwoCriticalHitCombination.includes(e.code)) {
        let isCombo = PlayerTwoCriticalHitCombination.every( item => pressed.get(item));
        if(!pressed.get(e.code)) {
          // ("Already bloked");
          return ;
        }else if(isCombo){
          // ('U+I+O');
          firstFighter = {...criticalHit(secondFighter, firstFighter)};
        }
    } else {
      
      if(pressed.get(PlayerTwoBlock) && pressed.get(PlayerOneAttack)){
          pressed.set(PlayerTwoAttack, false);
          // ('second in block, health saved');
      } else if( pressed.get(PlayerOneAttack) && pressed.get(PlayerTwoBlock)){
          // ("A + L");
          secondFighter = {...HitAndBlock(firstFighter, secondFighter)};
      } else if(pressed.get(PlayerOneBlock) && pressed.get(PlayerTwoAttack)){
          pressed.set(PlayerOneAttack, false);
          // ('first in block, health saved');
      } else if(pressed.get(PlayerTwoAttack) && pressed.get(PlayerOneBlock)){
          // ("J + D");
          firstFighter = {...HitAndBlock(secondFighter, firstFighter)};
      } else if(pressed.get(PlayerOneAttack)){
          // ("A");
          secondFighter = {...onlyHit(firstFighter, secondFighter)};
      } else if(pressed.get(PlayerTwoAttack)){
          // ("J");
          firstFighter = {...onlyHit(secondFighter, firstFighter)};
      }
    }
    
  });

  document.addEventListener('keyup', function(e) {
    if(PlayerOneCriticalHitCombination.includes(e.code) || PlayerTwoCriticalHitCombination.includes(e.code)) {
      
      pressed.set(e.code, false);
      
      setTimeout(()=> {
        pressed.delete(e.code);
        // ('Keys Free');
      }, 10000);
      return;
    }; 
    if(e.code == PlayerOneBlock ){
      pressed.delete(e.code);
      pressed.delete(PlayerOneAttack);
    }
    if(e.code == PlayerTwoBlock){
      pressed.delete(e.code);
      pressed.delete(PlayerTwoAttack);
    }else{
      // ('pressed: ', pressed);
      pressed.delete(e.code);
    }
       
  });

  function onlyHit(attacker, defender) {
    if(defender.health > 0) {
      defender.indicator.width = changeIndicatorWidth(defender.initHelth, defender.health);
      let dam = getHitPower(attacker);
      defender = {
        ...defender,
        health: Math.floor(defender.health - dam) > 0 ? Math.floor(defender.health - dam) : 0
      };
      defender.indicator.style.width = changeIndicatorWidth(defender.initHelth, defender.health);   
    } else {
      defender = {
        ...defender,
        health: 0         
      }; 
      defender.indicator.style.width = 0;
      winner = {...attacker};
      resolve(winner)     
    }
    return defender
  };

  function HitAndBlock(attacker, defender) { 
    if(defender.health > 0) {
      defender.indicator.width = changeIndicatorWidth(defender.initHelth, defender.health);
      let dam = getDamage(attacker, defender);

      defender = {
        ...defender,
        health: Math.floor(defender.health - dam) > 0 ? Math.floor(defender.health - dam) : 0
      };
      defender.indicator.style.width = changeIndicatorWidth(defender.initHelth, defender.health);   
    } else {
      defender = {
        ...defender,
        health: 0         
      }; 
      defender.indicator.style.width = 0; 
      winner = {...attacker};
      resolve(winner)     
    }
    return defender
  };

  function criticalHit(attacker, defender) {
    if(defender.health > 0) {
      defender.indicator.style.width = changeIndicatorWidth(defender.initHelth, defender.health); 
      let dam = 2 * attacker.attack;
      defender = {
        ...defender,
        health: Math.floor(defender.health - dam) > 0 ? Math.floor(defender.health - dam) : 0
      };
      defender.indicator.style.width = changeIndicatorWidth(defender.initHelth, defender.health); 
    } else {
      defender = {
        ...defender,
        health: 0         
      }; 
      defender.indicator.style.width = 0;       
      winner = {...attacker};
      resolve(winner)
    }
    return defender
  }

};

export async function fight(firstFighter, secondFighter) {  
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    checkWinner(firstFighter, secondFighter, resolve)    
  })
    .then(data => data);
}

export function getDamage(attacker, defender) {
  // attacker is perametr getHitPower(fighter)
  let damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;

}

export function getHitPower(fighter) {
  // return hit power
  let power = fighter.attack * getRndInteger(1, 2);
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  // dodge = random
  let power = fighter.defense * getRndInteger(1, 2);
  return power;
}

function getRndInteger(min, max) {
  return (Math.random() * (max - min) + min);
}


