import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  
  let winnerInfo = `
    <p class="winner__info_name"><strong>${fighter.name}</strong></p>
    <div class="winner__info_img">
      <img src="${fighter.source}" alt="">
    </div>
    
    <p class="winner__info_health"><strong>Health: ${fighter.health}</strong></p>
    <p class="winner__info_defense"><strong>Defense: ${fighter.defense}</strong></p>
    <p class="winner__info_attack"><strong>Attack: ${fighter.attack}</strong></p>
  `;
  let winnerElem = document.createElement('div');
  winnerElem.classList.add('winner__info');
  winnerElem.innerHTML = winnerInfo;
  let modalTitle = `WINNER!`;

  let dataModal = {
    title: modalTitle,
    bodyElement: winnerElem,
  };

  showModal(dataModal);
}
